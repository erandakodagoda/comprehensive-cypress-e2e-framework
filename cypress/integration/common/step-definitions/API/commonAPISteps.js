


Given('As a user I need to execute GET api for {string} and postcode {string}', (countrycode, postcode) => {
    cy.request({
        method: 'GET',
        url: `/`+countrycode+'/'+postcode,
        headers: {
            'Content-Type': 'application/json'
        },
        failOnStatusCode: false
    }).as('get_place_by_postcode')
});

Then('I verify the status code is {int}', (statuscode) =>{
    cy.get('@get_place_by_postcode').should((response)=>{
        expect(response.status).be.eq(statuscode);
    })
});
Then('I see the location country {string} and state {string}', (country, state) =>{
    cy.get('@get_place_by_postcode').should((response)=>{
        const countryname= response.body.country;
        assert.equal(countryname, country);

        const place= response.body.places[0].state;
        assert.equal(place, state);

    })
});
