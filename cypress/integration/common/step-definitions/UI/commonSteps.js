import LandingPage from '../../../pages/LandingPage/LandingPage'

const landingPage = new LandingPage();


Given('I am opening cypress.io on web browser', ()=> {
    cy.visit(`/todo`)
});
When('I add item {string} to todo application', (item) => {
    landingPage.addToDo(item);
    landingPage.saveValue();
});
Then('I can verify whether the item {string} is added as a todo', (item) => {
    landingPage.verifyToDoInTheList(item);
})
