var elements = require('./elements')

class LandingPage{
    addToDo(value){
        return cy.get(elements.LANDINGPAGE.TODO_TXT_FIELD).type(value);
        cy.log("Adding ToDo: ", value);
    }
    saveValue(){
        return cy.get(elements.LANDINGPAGE.TODO_TXT_FIELD).type('{enter}');
        cy.log("ToDo Saved");
    }
    verifyToDoInTheList(value){
        return cy.get(elements.LANDINGPAGE.TODO_LIST)
        .should('have.length', 3)
        .last()
        .should('have.text', value);
        cy.log("Validated ToDo is visible with: ", value);
    }
}

export default LandingPage;