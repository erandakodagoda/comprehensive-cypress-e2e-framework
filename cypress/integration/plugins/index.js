
/**
 * @type {Cypress.PluginConfig}
 */

const cucumber = require('cypress-cucumber-preprocessor').default;
const fs = require('fs-extra');
const path = require('path');


module.exports = (on, config) => {
    on('file:preprocessor', cucumber());

    function getConfigByFile(env){
        const pathToConfig = path.resolve("cypress/config", `${env}.config.json`);

        return fs.readJson(pathToConfig);
    }
    const env = config.env.configFile || "qa"; 
    
    return getConfigByFile(env);
}