@UI
Feature: Add ToDo
    As a user I should be able to navigate to cypress todo application and add a todo.

    Scenario Outline: Navigate to cypress.io and add a todo
        Given I am opening cypress.io on web browser
        When I add item '<item>' to todo application
        Then I can verify whether the item '<item>' is added as a todo

        Examples:
            | item               |
            | Wake Up at 8:00 am |

