@API
Feature: Verify API GET /{countrycode}/{post_code}
    As an API User I should be able to fetch location by postcode

    Scenario Outline: Verify API zippopotam.us and verify location API by postcode
        Given As a user I need to execute GET api for '<country_code>' and postcode '<post_code>'
        Then I verify the status code is 200
        And I see the location country '<country>' and state '<state>'

        Examples:
            | country_code | post_code | country       | state   |
            | us           | 33160     | United States | Florida |
