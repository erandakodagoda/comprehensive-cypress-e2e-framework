const cypress = require('cypress')
const marge = require('mochawesome-report-generator')
const { merge } = require('mochawesome-merge')
const moment = require('moment')

const currRunTimestamp = getTimeStamp();

const args = require('minimist')(process.argv.slice(3));


const sourceReport = {
    files: ["./reports/" + "Test Run - " + currRunTimestamp + "/mochawesome-report/*.json"],
}

const finalReport = {
    reportDir: 'reports/' + "Test Run - " + currRunTimestamp,
    saveJson: true,
    reportFilename: 'Run-Report',
    reportTitle: 'Run-Report',
    reportPageTitle: 'Run-Report'
}

cypress.run({   
    ...args,                           
    config: {
        pageLoadTimeout: 10000,
        screenshotsFolder: 'reports/' + "Test Run - " + currRunTimestamp + '/screenshots',
        video: true,
        videosFolder: 'reports/' + "Test Run - " + currRunTimestamp + '/videos'
    },
    reporter: 'mochawesome',
    reporterOptions: {
        reportDir: 'reports/' + "Test Run - " + currRunTimestamp + '/automation-report',
        overwrite: false,
        html: true,
        json: true
    }
}).then(result => {

    generateReport()
    .then(() => {
        console.log("All Reports merged");
    })
    .catch(err => {
        console.error("Getting error while merging reports: ", err.message)

    })
    .finally(() => {
        console.log("Test Run Completed at - "+ result.endedTestsAt);
      })
  
    })
.catch(err => {
    generateReport()
    console.error(err.message)
    process.exit(1)
  })

function getTimeStamp() {
    var now = new moment().format('DD-MM-YYYY--HH_mm_ss')
    return now
}

function generateReport() {
    return  merge(sourceReport).then(report => {marge.create(report, finalReport)});
    
}

function getEnvironment(args){

    let environment;
    let getEnv;
   
     if(args.env){
       if(args.env === true){
           environment = "default";
           return "default";
       }
   
       if(Array.isArray(args.env)){
           getEnv = args.env.join(",").split(",");
       }else{
           getEnv = args.env.split(",");
       }
   
     getEnv.map((curr, index) => {
   
       const envProperty = curr.split("=");
   
       if(envProperty[0] === 'configFile'){
           environment = envProperty[1];
       }
   
       if(index >= getEnv.length){
           environment = "default";
       }
   
    })
   
    if(environment === undefined){
       environment = "default";
   }
   
    return environment;
   
   } else{
       environment = "default";
       return "default";
    }
   }