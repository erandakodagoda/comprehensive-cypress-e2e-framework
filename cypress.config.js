const { defineConfig } = require("cypress");

module.exports = defineConfig({
  reporter: 'mochawesome',
  reporterOptions: {
    overwrite: true,
    html: true,
    json: true
  },
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
      return require('./cypress/integration/plugins/index.js')(on, config)
    },
    specPattern: 'cypress/e2e/**/features/**/*.{feature, features}',
    excludeSpecPattern: '**/pages/*.**/common/*',
    experimentalSessionAndOrigin: false
  },
});
